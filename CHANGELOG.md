CHANGELOG
=========

1.0.0 (2016-05-16)
------------------

* first release version
* remove tests from composer - not used yet

0.2.0 (2016-05-15)
------------------

* updated createCustomer to add created_at parameter
* removed email from updateCustomer parameters (specify via attributes if required)
* added parameter validation to trackEvent
* new function trackPageview
* new function trackAnonymousEvent
* added new timestamp parameter to CustomerIo::trackEvent to optionally set timestamp on event

0.1.0 (2016-05-15)
------------------

* first working version
