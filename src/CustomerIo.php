<?php namespace CustomerIo;

use CustomerIo\Exception\InvalidArgumentException;

class CustomerIo
{
	/** @var CustomerIoClient our client implementation */
	protected $client;

	function __construct(CustomerIoClient $client)
	{
		$this->client = $client;
	}

	/**
	 * note that $email and $created_at will override $attributes['email'] and $attributes['created_at'] if set
	 *
	 * @param $customer_id
	 * @param $email
	 * @param $created_at
	 * @param array $attributes
	 * @param bool $update
	 *
	 * @return int status code
	 */
	public function createCustomer($customer_id, $email, $created_at, array $attributes = [], $update = false)
	{
		if (isset($email))
		{
			if (!$this->validateEmail($email))
			{
				throw new InvalidArgumentException("Invalid email address [{$email}]");
			}

			$attributes['email'] = $email;
		}
		elseif (!$update)
		{
			throw new InvalidArgumentException("Email address required for new customer");
		}

		if (isset($created_at))
		{
			$attributes['created_at'] = $created_at;
		}
		elseif (!$update)
		{
			throw new InvalidArgumentException("Created_at timestamp required for new customer");
		}

		return $this->client->put("customers/{$customer_id}", $attributes);
	}

	/**
	 * Note: pre-defined attributes which can be updated:
	 *
	 * 		$attributes['email']
	 * 		$attributes['created_at']
	 *
	 * @param $customer_id
	 * @param array $attributes
	 *
	 * @return int status code
	 */
	public function updateCustomer($customer_id, array $attributes = [])
	{
		return $this->createCustomer($customer_id, null, null, $attributes, true);
	}

	public function deleteCustomer($customer_id)
	{
		return $this->client->delete("customers/{$customer_id}");
	}

	/**
	 * Note: if no timestamp specified, current time will be used
	 *
	 * @param $customer_id
	 * @param $event_name
	 * @param null $timestamp
	 * @param array $data
	 *
	 * @return int status code
	 */
	public function trackEvent($customer_id, $event_name, $timestamp = null, array $data = [])
	{
		if (empty($event_name))
		{
			throw new InvalidArgumentException("Event name is required");
		}

		$payload['name'] = $event_name;
		if (!empty($data)) $payload['data'] = $data;
		if (isset($timestamp)) $payload['timestamp'] = $timestamp;

		return $this->client->post("customers/{$customer_id}/events", $payload);
	}

	public function trackPageview($customer_id, $url, $referrer = '')
	{
		if (empty($url))
		{
			throw new InvalidArgumentException("URL is required");
		}

		$payload['name'] = $url;
		$payload['type'] = 'page';
		if (!empty($referrer)) $payload['data'] = ['referrer' => $referrer];

		return $this->client->post("customers/{$customer_id}/events", $payload);
	}

	public function trackAnonymousEvent($event_name, $recipient, $timestamp = null, array $data = [])
	{
		if (empty($event_name))
		{
			throw new InvalidArgumentException("Event name is required");
		}

		if (empty($recipient))
		{
			throw new InvalidArgumentException("Recipient email is required");
		}

		if (!$this->validateEmail($recipient))
		{
			throw new InvalidArgumentException("Invalid email address [{$recipient}]");
		}

		$payload['name'] = $event_name;
		if (!empty($data)) $payload['data'] = $data;
		$payload['data']['recipient'] = $recipient;
		if (isset($timestamp)) $payload['timestamp'] = $timestamp;

		return $this->client->post("events", $payload);
	}

	private function validateEmail($email)
	{
		$filtered = filter_var($email, FILTER_VALIDATE_EMAIL);
		if ($filtered === false)
		{
			return false;
		}

		return true;
	}
}
