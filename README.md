Customer.io Service
===================

A Customer.io API wrapper using Guzzle v6.

By [Simon Hampel](http://hampelgroup.com/).

Installation
------------

The recommended way of installing Customer.io Service is through [Composer](http://getcomposer.org):

    :::json
    {
        "require": {
            "hampel/customerio": "~1.0"
        }
    }

Configuration
-------------

TODO
